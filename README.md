# WordPress GDPR Consent Class 

---

### According to request:

> *Please create a composer package, which serves the functionality working with WordPress Nonces. That means to have at least wp_nonce_\*() function implemented in an object orientated way. You don't have to replace the current functionality, just implement the WordPress Nonces more like OOP. README is mandatory, UnitTests desired.*

---

## Description 

WordPress class that helps collecting GDPR consent expressed by authorized user. The consent is stored among user's meta values in WP. 

Object displays appropriate messages depending on actual action, constent's status and version (if most recent or not).

Object refers to the current WP user.

--- 

## Installation 

composer require mmi_1/gdpr_consent:dev-master

--- 

## Usage 

**1. Data preparation:**

Edit data/consent_data.json and deploy appropriate values for actual consent: version and text (html allowed).

**2. Include the code:**

In the php file in your template please use the following declaration at the beginning of file:

```
<?php
	use User\GdprConsent;
	require_once('/Path_to/autoload.php');
?>
```

**3. Instantiate object:**

```
<?php $consent = new GdprConsent(); ?>
```

**4. Create form:**

```
<form action="">
    <?php echo $consent->render($_GET); ?>
</form>
```

---

## Available methods:

**isChecked()** - If any consent's version has ever been accepted 

**isCheckedAndUpToDate()** - If current consent's version has been accepted 

**currentVersion()** - number of current version of the clause (consent_data.json)

**currentText()** - text of current version of the clause (consent_data.json)

**checkedVersion()** - Number of consent's accepted version

**checkedTimestamp()** - timestamp when consent was submitted 

