<?php
declare(strict_types=1);

namespace User;

use PHPUnit\Framework\TestCase;

final class GdprConsentTest extends TestCase
{

    protected $consent;
    protected $rootClassPath;

    public function setUp()
    {
        $this->consent = new GdprConsent();

        $class = GdprConsent::class;
        $loader = require './../vendor/autoload.php';
        $classPath = $loader->findFile($class);
        $realClassPath = str_replace('GdprConsent.php', '', realpath($classPath));
        $this->rootClassPath = realpath($realClassPath . '../');
    }

    public function testIsClass(): void
    {
        $this->assertInstanceOf(GdprConsent::class, new GdprConsent());
    }

    public function testIfDataFileExists(): void
    {
        $this->assertFileExists($this->rootClassPath . '/data/consent_data.json');
    }

    public function testIfDataFileIsProperJson(): void
    {
        try {
            $file = file_get_contents($this->rootClassPath . '/data/consent_data.json');
        } catch (Exception $exception) {
            $exception->getMessage();
        }

        $this->assertJson($file);
    }

    public function testIsVersionAFloat(): void
    {
        $this->assertInternalType('float', $this->consent->currentVersion());
    }

    public function testIsTextAString(): void
    {
        $this->assertInternalType('string', $this->consent->currentText());
    }

    /**
     * This should be assertion checking if the output is proper HTML. Needs additional PHPUnit plugin
     * @throws Exception
     */
//    public function testRenderIsReturnHtml(): void
//    {
//        $consent = new GdprConsent();
//        $return = $consent->render($_GET);
//
////        assertEquals(true, true);
//    }
}
