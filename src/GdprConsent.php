<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 *
 * User: Marek
 * Date: 2018-12-09
 * Time: 14:10
 */

namespace User;

class GdprConsent
{
    /**
     * Current user id
     * @var int
     */
    private $userId;

    /**
     * Default action name
     * @var string
     */
    private $action;

    /**
     * Default action name
     * @var string
     */
    private $wpLoadPath;

    /**
     * Current version of actual GDPR Clause
     * @var float
     */
    private $currentVersion;

    /**
     * Text of actual GDPR Clause (html allowed)
     * @var string
     */
    private $currentText;

    /**
     * Info if consent is checked
     * @var bool
     */
    private $isChecked;

    /**
     * Info if consent is checked for the current version of GDPR Clause
     * @var bool
     */
    private $isCheckedAndUpToDate;

    /**
     * Number of version that has been approved by user
     * @var float
     */
    private $checkedVersion;

    /**
     * Transaction timestamp for user's approval
     * @var int
     */
    private $checkedTimestamp;

    /**
     * GdprConsent Class constructor
     */
    public function __construct()
    {
        $this->isChecked = false;
        $this->isCheckedAndUpToDate = false;
        $this->currentVersion = 0;
        $this->currentText = '';
        $this->action = 'gdpr_consent_action';
        $this->wpLoadPath = '';

        $file = '/wp-load.php';
        $path = '';

        for ($i = 0; $i < 100; $i++) {
            $path .= '/..';
            if (file_exists(__DIR__ . $path . $file)) {
                $this->wpLoadPath = __DIR__ . $path;
            }
        }

        $file = $this->wpFileGetContents(__DIR__ . '/../data/consent_data.json');
        $file = json_decode($file, true);

        if (isset($file)) {
            if (isset($file['version'])) {
                $this->currentVersion = (float)number_format(floatval($file['version']), 2, '.', '');
            }
            if (isset($file['text'])) {
                $this->currentText = (string)$file['text'];
            }
        }

        $userId = $this->getWpFunction('get_current_user_id');
        if (isset($userId)) {
            $this->userId = (int)$userId;
        }

        $this->checkedVersion = (float)floatval($this->userMeta('GDPR_Consent_checked_version'));
        if ($this->checkedVersion > 0) {
            $this->isChecked = true;
        }

        if ($this->isChecked and $this->checkedVersion === $this->currentVersion) {
            $this->isCheckedAndUpToDate = true;
        }
        $this->checkedTimestamp = (int)$this->userMeta('GDPR_Consent_checked_timestamp');
    }

    /**
     * Method to render HTML with nonce
     * @param $get array
     * @return string
     */
    public function render(array $get): string
    {
        $checkedInfo = '<p>Tick the checkbox and get the most recent offers!</p>';

        if ($this->userId <= 0) {
            return 'Please log in.';
        }
        if (isset($get) and isset($get['__wpnonce']) and $this->getWpFunction(
            'wp_verify_nonce',
            [$get['__wpnonce'], $this->action]
        )) {
            $consent = 0;
            if (isset($get['_consent'])) {
                $consent = $get['_consent'];
            }

            $html = '<div>You have no acces to data!</div>';
            if ($this->userHasPrivileges()) {
                $this->getWpFunction(
                    'update_user_meta',
                    [ $this->userId, 'GDPR_Consent_checked_version', $consent ]
                );
                $this->getWpFunction(
                    'update_user_meta',
                    [ $this->userId, 'GDPR_Consent_checked_timestamp', (new \DateTime())->getTimestamp() ]
                );
                $html = '<div>Thank you for sending your consent!</div>';
            }
            return $html;
        }

        $timestamp = date(
            'd/m/Y H:i:s',
            $this->checkedTimestamp
        );
        $checkedAttr = ($this->isChecked) ? 'checked' : '';
        if ($this->isCheckedAndUpToDate) {
            $checkedInfo = '<p>You expressed consent on: ' . $timestamp . '. You may recall it anytime. </p>';
            $checkedAttr = 'checked';
        } elseif ($this->isChecked) {
            $checkedSufffix = 'Due to privacy policy update, please submit your consent again. Thank you!';
            $checkedInfo = '<p>You expressed consent on: ' . $timestamp . ' ' . $checkedSufffix .'</p>';
            $checkedAttr = '';
        }

        $consentInput = '<input type="checkbox" name="_consent" ' . $checkedAttr . ' value="' .
            $this->currentVersion . '"> ';
        $nonceInput = $this->getWpFunction(
            'wp_nonce_field',
            [ $this->action, '__wpnonce', true, false ]
        );
        $currentText = '<div>' . $this->currentText . '</div>';
        $button = '<div><button type="submit">Save</button></div>';
        $html = $consentInput . $nonceInput . $currentText . $checkedInfo . $button;

        return $html;
    }

    /**
     * Returns user meta
     *
     * @param $meta string
     * @return string
     */
    public function userMeta(string $meta): string
    {
        $userMeta = '';

        if (null !== $this->getWpFunction('get_user_meta', [$this->userId, $meta, true])) {
            $userMeta .= $this->getWpFunction('get_user_meta', [ $this->userId, $meta, true ]);
        }

        return $userMeta;
    }

    /**
     * Checks whether current user has privileges to update their account data
     *
     * @return bool
     */
    public function userHasPrivileges(): bool
    {
        // wp functions to check if user may update their meta data

        return true;
    }

    /**
     * @return float
     */
    public function currentVersion(): float
    {
        return floatval($this->currentVersion);
    }

    /**
     * @return string
     */
    public function currentText(): string
    {
        return $this->currentText;
    }

    /**
     * @return bool
     */
    public function isChecked(): bool
    {
        return $this->isChecked;
    }

    /**
     * @return bool
     */
    public function isCheckedAndUpToDate(): bool
    {
        return $this->isCheckedAndUpToDate;
    }

    /**
     * @return float
     */
    public function checkedVersion(): float
    {
        return $this->checkedVersion;
    }

    /**
     * @return int
     */
    public function checkedTimestamp(): int
    {
        return $this->checkedTimestamp;
    }

    /**
     * php file_get_contents that works for WP
     * @param string $path_file
     *
     * @return string
     */
    private function wpFileGetContents(string $pathFile = ''): string
    {
        ob_start();

        if (!file_exists($pathFile)) {
            return '';
        }

        include $pathFile;
        $fileGetContents = ob_get_clean();

        return $fileGetContents;
    }

    /**
     * WP function execution
     * @param $func string
     * @param $args array
     *
     * @return mixed
     */
    private function getWpFunction(string $func = '', array $args = [])
    {
        require(realpath($this->wpLoadPath . '/wp-load.php'));

        return call_user_func_array($func, $args);
    }
}
